# Ability to do Transformations With Apache Pig

## What is Apache Pig ?

Map Reduce requires programs to be translated into map and reduce stages. Since not all data analysts were familiar with Map Reduce, hence, Apache pig was introduced by Yahoo researchers to bridge the gap. The Pig was built on top of Hadoop that provides a high level of abstraction and enables programmers to spend less time writing complex Map Reduce programs. Pig is not an acronym; it was named after a domestic animal. As an animal pig eats anything, Pig can work upon any kind of data.

## Apach Pig Applications

* Processes large volume of data
* Supports quick prototyping and ad-hoc queries across large datasets
* Performs data processing in search platforms
* Processes time-sensitive data loads
* Used by telecom companies to de-identify the user call data information.

![](https://gitlab.com/abhijain7417/apache-pig/-/raw/master/Source/Pig1.png?inline=false)

![](https://gitlab.com/abhijain7417/apache-pig/-/raw/master/Screenshot__47_.png?inline=false)

### Pig Complements HIVE

![](https://gitlab.com/abhijain7417/apache-pig/-/raw/master/Screenshot__48_.png?inline=false)

### ETL(Extrct Transform Load)

![](https://gitlab.com/abhijain7417/apache-pig/-/raw/master/Screenshot__49_.png?inline=false)

### Pig Latin

![](https://gitlab.com/abhijain7417/apache-pig/-/raw/master/Screenshot__50_.png?inline=false)

### Difference Between Pig Vs SQL

![](https://gitlab.com/abhijain7417/apache-pig/-/raw/master/Screenshot__51_.png?inline=false)

### Difference Between Pig Vs HIVE

![](https://gitlab.com/abhijain7417/apache-pig/-/raw/master/Screenshot__52_.png?inline=false)

# KO1: Able to use GRUNT Shell

1. Download the Apache Pig from www.apache.org

![](Screenshot__53_.png)

2. Change the Environment Settings

![](Screenshot_from_2021-05-16_18-40-53.png)

![](Screenshot_from_2021-05-16_18-42-06.png)

3. PIG Modes of Operation

![](Screenshot__54_.png)

![](Screenshot__55_.png)

![](Screenshot__56_.png)

![](Screenshot__57_.png)

![](Screenshot__59_.png)

4. How to start the GRUNT Shell

![](Screenshot_from_2021-05-16_20-23-04.png)

5. Load the file using PigStorage

![](Screenshot_from_2021-05-16_20-27-07.png)

6. Exit the GRUNT Shell

![](Screenshot_from_2021-05-16_20-27-49.png)

# KO2: Able to load data into Relations

1. How Pig Works

![](Screenshot__61_.png)

2. What is Relation in Pig

![](Screenshot__62_.png)

3. pig as a Data Flow Language

![](Screenshot__63_.png)

4. Load Data from Files

![](Screenshot_from_2021-05-16_20-56-30.png)

![](Screenshot_from_2021-05-16_20-56-46.png)

5. Pig Data types

![](Screenshot__64_.png)

* Scalar Data types

![](Screenshot__65_.png)

* Complex Data types

![](Screenshot__66_.png)

# KO3: Able to work with Data Transformations

1. ForEach in Pig

![](Screenshot__67_.png)

2. Functions in Pig

![](Screenshot__68_.png)

![](Screenshot__69_.png)

3. Conditional Operators in Pig

![](Screenshot__70_.png)

4. Distinct,Limit And Supports

![](Screenshot__71_.png)

5. Split in Pig

![](Screenshot__72_.png)




















